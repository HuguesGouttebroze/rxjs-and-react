import { Observable } from 'rxjs';
import BaseRequestModel from '../utils/base-request-model';
import { Body } from '../utils/types';

export const Api = {
    // get request
    get: (route: string): Observable<any> => {
        const headers = {
            'Access-Control-Allow-Origin': '*'
        };
        const newBase = new BaseRequestModel(route, 'GET', headers);
        return newBase.request();
    },
    // post request
    post: (route: string, form: Body): Observable<any> => {
        const headers = {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        };
        const newBase = new BaseRequestModel(route, 'POST', headers, form)
        return newBase.request();
    },
};
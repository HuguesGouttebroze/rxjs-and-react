import { Method, _Headers, Body } from "./types";

export interface Props {
    url: string;
    method?: Method;
    headers: _Headers;
    body?: Body;
}
